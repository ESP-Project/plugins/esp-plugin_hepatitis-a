'''
                                  ESP Health
                                 Hepatitis A
                              Disease Definition


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://www.esphealth.org
@copyright: (c) 2011-2012 Channing Laboratory
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
#
# This may not still be true in newer versions of Django - JM 6 Dec 2011
from datetime import timedelta

from django.db.models import F

from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.hef.base import LabResultPositiveHeuristic
from ESP.hef.base import LabResultRatioHeuristic
from ESP.hef.models import Event
from ESP.nodis.base import DiseaseDefinition
from ESP.utils import log


class Hepatitis_A(DiseaseDefinition):
    '''
    Hepatitis A
    '''

    # A future version of this disease definition may also detect chronic hep a
    conditions = ['hepatitis_a:acute']

    uri = 'urn:x-esphealth:disease:channing:hepatitis_a:v1'

    short_name = 'hepatitis_a:acute'

    test_name_search_strings = [
        'hep',
        'alt',
        'ast',
        'bili',
        'tbil',
        'hbv',
        'hcv',
        'sgpt',
        'sgot',
        'aminotrans'
    ]

    timespan_heuristics = []

    @property
    def event_heuristics(self):
        """
        Event heuristics used by all Hepatitis variants
        """
        heuristic_list = []
        heuristic_list.append(DiagnosisHeuristic(
            name='jaundice',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='R17', type='icd10'),
                Dx_CodeQuery(starts_with='782.4', type='icd9'),
            ]
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_a_igm_antibody',
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='ast',
            ratio=2,
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='alt',
            ratio=2,
        ))

        return heuristic_list

    def generate(self):
        log.info('Generating cases for %s (%s)' % (self.short_name, self.uri))
        #
        # Acute Hepatitis A
        #
        # (dx:jaundice or lx:alt:ratio:2.0 or lx:ast:ratio:2.0) 
        # AND lx:hepatitis_a_igm_antibody:positive within 14 days
        #
        primary_event_name = 'lx:hepatitis_a_igm_antibody:positive'
        secondary_event_names = [
            'dx:jaundice',
            'lx:alt:ratio:2',
            'lx:ast:ratio:2',
        ]
        event_qs = Event.objects.filter(
            name=primary_event_name,
            patient__event__name__in=secondary_event_names,
            patient__event__date__gte=(F('date') - timedelta(days=14)),
            patient__event__date__lte=(F('date') + timedelta(days=14)),
        )
        relevant_event_names = [primary_event_name] + secondary_event_names
        counter = self._create_cases_from_event_qs(
            condition='hepatitis_a:acute',
            criteria='Criteria #1: [(jaundice (not of newborn) or (alt or ast) > 2x ULN) and positive of hep_a_igm] w/in 14 days',
            recurrence_interval=None,
            event_qs=event_qs,
            relevant_event_names=relevant_event_names,
        )
        log.debug('Created %s new Hep A ' % counter)
        return counter

    def report_field(self, report_field, case):
        reportable_fields = {
            'NA-56': 'NA-1739',
            '10187-3': 'NA-1738',
            'symptom_obx': False,
            'na_trmt_obx': False,
        }

        return reportable_fields.get(report_field, None)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Packaging
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def event_heuristics():
    return Hepatitis_A().event_heuristics


def disease_definitions():
    return [Hepatitis_A()]
